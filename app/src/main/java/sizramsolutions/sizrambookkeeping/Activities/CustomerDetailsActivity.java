package sizramsolutions.sizrambookkeeping.Activities;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Adapters.CustomerDetailsAdapter;
import sizramsolutions.sizrambookkeeping.Fragments.TabTodayFragment;
import sizramsolutions.sizrambookkeeping.Models.CustomerDetails;
import sizramsolutions.sizrambookkeeping.Models.CustomerList;
import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class CustomerDetailsActivity extends AppCompatActivity {


    private static final String TAG = CustomerDetailsActivity.class.getSimpleName();

    private List<CustomerDetails> customerstatement = new ArrayList<>();

  //  double sellAmount, purchaseAmount, receivedAmount, paidAmount, receivableAmount, payableAmount, stockAmount, expenseAmount, openingUnitAmount, openingItemAmount, purchaseUnitAmount, purchaseItemAmount, saleUnitAmount, saleItemAmount;

    RequestQueue requestQueue;

    Toolbar customerDetailsToolbar;
    CustomerDetailsAdapter customerDetailsAdapter;

    TextView customerName, customerPhone, customerAddress, customerBalance;

    //Just declare it (customerId) here
    //previously your getIntent() was here, but Intent can only be found when Activity is created
    //that means you can evoke getIntent() in or after onCreate()  --- Tahmid
    int customerId ;

    String URL_CUSTOMER_TRANSACTION = "http://ss-app01.com/shinutrade/index.php/Android_api/customers_statement/"+customerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);

        customerDetailsToolbar = (Toolbar)findViewById(R.id.customer_details_toolbar);
        customerName = (TextView)findViewById(R.id.customer_name);
        customerPhone = (TextView)findViewById(R.id.customer_phone);
        customerAddress = (TextView)findViewById(R.id.customer_address);
        customerBalance = (TextView)findViewById(R.id.customer_balance);

        //Please have a look at life cycle methods in official doc
        //See here is your getIntent() & it works  ---Tahmid
        customerId = getIntent().getIntExtra("customer_id", -1);
        RecyclerView customerDetailsRecycler = (RecyclerView)findViewById(R.id.customer_transaction_list);
        customerDetailsRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager customerDetailsListLlm = new LinearLayoutManager(getApplicationContext());
        customerDetailsRecycler.setLayoutManager(customerDetailsListLlm);

        ArrayList<CustomerDetails> customerDetailsListArray = new ArrayList<CustomerDetails>();

        CustomerDetailsAdapter customerDetailsAdapter = new CustomerDetailsAdapter(getApplicationContext(), customerDetailsListArray);
        customerDetailsRecycler.setAdapter(customerDetailsAdapter);

        customerDetailsData();


    }

    public void customerDetailsData() {
        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getApplicationContext()).getRequestQueue();

        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_CUSTOMER_TRANSACTION,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray customerStatementArray) {

                        for(int i =0; i < customerStatementArray.length(); i++){

                            try {
                                JSONObject customerStatementObject = customerStatementArray.getJSONObject(i);


                                CustomerDetails customerStatement = new CustomerDetails(
                                        customerStatementObject.optString("transaction_date", "not Found"),
                                        customerStatementObject.optInt("invoice_number", 0),
                                        customerStatementObject.optDouble("total_price", 0),
                                        customerStatementObject.optDouble("invoice_paid_amount", 0),
                                        customerStatementObject.optDouble("discount_amount", 0)

                                );

                                customerstatement.add(customerStatement);



                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),
                                        "exception Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        "Response Error: " + error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(jsonArrayRequest);
    }


}
