package sizramsolutions.sizrambookkeeping.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import sizramsolutions.sizrambookkeeping.Fragments.CustomerListFragment;
import sizramsolutions.sizrambookkeeping.Fragments.DashboardFragment;
import sizramsolutions.sizrambookkeeping.Fragments.LowStockFragment;
import sizramsolutions.sizrambookkeeping.Fragments.ProductListFragment;
import sizramsolutions.sizrambookkeeping.Fragments.PurchaseFragment;
import sizramsolutions.sizrambookkeeping.Fragments.SaleFragment;
import sizramsolutions.sizrambookkeeping.Fragments.VendorListFragment;
import sizramsolutions.sizrambookkeeping.R;

public class DashboardActivity extends AppCompatActivity {

    DrawerLayout drawer;
    NavigationView navigationView;
    FragmentManager FM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FM= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=FM.beginTransaction();
        fragmentTransaction.replace(R.id.content_main,new DashboardFragment()).commit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawer.closeDrawers();
                if (item.getItemId()==R.id.dashboard)
                {
                    FragmentTransaction fragmentTransaction=FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main,new DashboardFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId()==R.id.customer){
                    FragmentTransaction fragmentTransaction=FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main,new CustomerListFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId()==R.id.vendor){
                    FragmentTransaction fragmentTransaction=FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main,new VendorListFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId()==R.id.stock){
                    FragmentTransaction fragmentTransaction=FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main,new ProductListFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId()==R.id.low_stock){
                    FragmentTransaction fragmentTransaction=FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main,new LowStockFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId()==R.id.sale) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new SaleFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId()==R.id.purchase) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new PurchaseFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();

                }

                return false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
