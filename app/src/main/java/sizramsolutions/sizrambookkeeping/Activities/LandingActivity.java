package sizramsolutions.sizrambookkeeping.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.Adapters.ScreenSlideAdapter;

public class LandingActivity extends FragmentActivity {

    ViewPager viewpager;
    ScreenSlideAdapter adapter;
    TabLayout tabLayout;
    Button startButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        adapter = new ScreenSlideAdapter(getSupportFragmentManager(),this);

        viewpager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tab_layout);

        viewpager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewpager);

        startButton = findViewById(R.id.start_btn);


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_register = new Intent(LandingActivity.this, RegisterActivity.class);
                startActivity(intent_register);
            }
        });

        disableTabStrip();

    }

    public void disableTabStrip() {
        LinearLayout tabStrip = (LinearLayout) tabLayout.getChildAt(0);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

}
