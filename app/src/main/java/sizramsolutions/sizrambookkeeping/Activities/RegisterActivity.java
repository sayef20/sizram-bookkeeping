package sizramsolutions.sizrambookkeeping.Activities;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sizramsolutions.sizrambookkeeping.R;

public class RegisterActivity extends AppCompatActivity {

    Button login;
    EditText editEmail, editPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //register = (Button) findViewById(R.id.register_btn);
        login = (Button) findViewById(R.id.login_btn);

       // editUserName = (EditText) findViewById(R.id.userName);
        editEmail = (EditText) findViewById(R.id.email);
        editPass = (EditText) findViewById(R.id.password);
       // editCompany = (EditText) findViewById(R.id.company);
        //editCompanyId = (EditText) findViewById(R.id.companyId);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }


/*        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder loginBuilder = new AlertDialog.Builder(RegisterActivity.this);
                View loginview = getLayoutInflater().inflate(R.layout.login_dialogue, null);
                final EditText loginEmail = loginview.findViewById(R.id.loginEmail);
                final EditText loginPassword = loginview.findViewById(R.id.loginPassword);
                Button loginBtn = loginview.findViewById(R.id.login_btn);

                loginBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!loginEmail.getText().toString().isEmpty() && !loginPassword.getText().toString().isEmpty()) {

                            Toast.makeText(RegisterActivity.this, R.string.login_successful, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RegisterActivity.this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                loginBuilder.setView(loginview);
                AlertDialog dialog = loginBuilder.create();
                dialog.show();
            }
        });*/

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent method to just to work on dashboard. Later to be replaced //
                Intent intent_dashboard = new Intent(RegisterActivity.this, DashboardActivity.class);
                startActivity(intent_dashboard);
            }

        });

    }

    // Override method for Adding Back Button //
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
