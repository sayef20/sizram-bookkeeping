package sizramsolutions.sizrambookkeeping.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.sizrambookkeeping.Models.CustomerDetails;
import sizramsolutions.sizrambookkeeping.R;


public class CustomerDetailsAdapter extends RecyclerView.Adapter<CustomerDetailsAdapter.CustomerDetailsViewHolder>{

    private Context customerDetailsContext;
    private List<CustomerDetails> customerDetailsList;



    public class CustomerDetailsViewHolder extends RecyclerView.ViewHolder{

        public TextView customerTransactionDate, customerTransactionInvoice, customerTransactionTotal, customerTransactionPaid, customerTransactionDue;

        public CustomerDetailsViewHolder(View view){

            super(view);

            customerTransactionDate = (TextView)view.findViewById(R.id.customer_trans_invoice_date);
            customerTransactionInvoice = (TextView)view.findViewById(R.id.customer_trans_invoice_no);
            customerTransactionTotal = (TextView)view.findViewById(R.id.customer_trans_total_amount);
            customerTransactionPaid = (TextView)view.findViewById(R.id.customer_trans_paid_amount);
            customerTransactionDue = (TextView)view.findViewById(R.id.customer_trans_due_amount);
        }

    }


    public CustomerDetailsAdapter(Context customerDetailsContext, List<CustomerDetails> customerDetailsList){

        this.customerDetailsContext = customerDetailsContext;
        this.customerDetailsList = customerDetailsList;
    }


    @Override
    public CustomerDetailsAdapter.CustomerDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View customerDetailsView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_customer_transaction, parent, false);
        return new CustomerDetailsAdapter.CustomerDetailsViewHolder(customerDetailsView);
    }


    @Override
    public void onBindViewHolder(final CustomerDetailsAdapter.CustomerDetailsViewHolder holder, int position) {
        CustomerDetails customerdetails = customerDetailsList.get(position);

        holder.customerTransactionDate.setText("" + customerdetails.getCustomerTransactionDate());
        holder.customerTransactionInvoice.setText("" + customerdetails.getCustomerTransactionInvoiceNo());
        holder.customerTransactionTotal.setText("" + customerdetails.getCustomerTransactionAmount());
        holder.customerTransactionPaid.setText("" + customerdetails.getCustomerTransactionPaid());
        holder.customerTransactionDue.setText("" + customerdetails.getCustomerTransactionDue());
    }

    @Override
    public int getItemCount() {
        return customerDetailsList.size();
    }
}
