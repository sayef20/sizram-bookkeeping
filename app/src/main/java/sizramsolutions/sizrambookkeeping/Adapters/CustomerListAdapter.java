package sizramsolutions.sizrambookkeeping.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.sizrambookkeeping.Activities.CustomerDetailsActivity;
import sizramsolutions.sizrambookkeeping.Models.CustomerList;
import sizramsolutions.sizrambookkeeping.R;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListViewHolder> {
    private List<CustomerList> customerList;
    private Context customerListContext;

    public CustomerListAdapter(List<CustomerList> customerList, Context customerListContext){
        this.customerList = customerList;
        this.customerListContext = customerListContext;

    }



 /*   private CustomerClickListener customerClickListener;
    //Context customerListContext;


    // ClickListener Interface //
    public static interface CustomerClickListener {
       void onItemClick(int position);   //done
    }

   // setonClickListener Method //
   public void setOnclickListener(CustomerClickListener listener){
       customerClickListener = listener;   //done

    }*/



    public class CustomerListViewHolder extends RecyclerView.ViewHolder{

        public TextView customerName, customerPhone, customerLastTransaction, customerDue;

        public CustomerListViewHolder(View view){

            super(view);
            customerName = (TextView)view.findViewById(R.id.customer_name);
            customerPhone = (TextView)view.findViewById(R.id.customer_phone);
            customerLastTransaction = (TextView)view.findViewById(R.id.last_customer_transaction);
            customerDue = (TextView)view.findViewById(R.id.customer_due);

        }
    }


    @Override
    public CustomerListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View customerListView = LayoutInflater.from(customerListContext).inflate(R.layout.customer_list_item, parent, false);
        return new CustomerListViewHolder(customerListView);
    }


    @Override
    public void onBindViewHolder(CustomerListViewHolder holder, final int position) {
        final CustomerList customerlist = customerList.get(position);

       View customerListView = holder.itemView;

        holder.customerName.setText(""+ customerlist.getCustomerName()+"");
        holder.customerPhone.setText("Phone: "+ customerlist.getCustomerPhone()+"");
        holder.customerLastTransaction.setText("Last: "+ customerlist.getCustomerLastTransaction()+" Tk.");
        holder.customerDue.setText("Current: " + customerlist.getCustomerDue()+" Tk.");


        customerListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(customerListContext, CustomerDetailsActivity.class);
                intent.putExtra("customer_id", customerlist.getCustomerId());

                customerListContext.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }
}


