package sizramsolutions.sizrambookkeeping.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Models.ProductList;
import sizramsolutions.sizrambookkeeping.Models.PurchaseList;
import sizramsolutions.sizrambookkeeping.Models.SaleList;
import sizramsolutions.sizrambookkeeping.R;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {

    private List<ProductList> productList;
    private Context productListContext;

    public ProductListAdapter(List<ProductList> productList, Context productListContext) {
        this.productList = productList;
        this.productListContext = productListContext;

    }

    public class ProductListViewHolder extends RecyclerView.ViewHolder {

        public TextView productCode, productName, productQuantity, productPurchasePrice, productSellingPrice;

        public ProductListViewHolder(View view) {

            super(view);
            productCode = (TextView) view.findViewById(R.id.product_code);
            productName = (TextView) view.findViewById(R.id.product_name);
            productQuantity = (TextView) view.findViewById(R.id.product_qty);
            productPurchasePrice = (TextView) view.findViewById(R.id.product_purchase_price);
            productSellingPrice = (TextView) view.findViewById(R.id.product_selling_price);

        }

    }


    @Override
    public ProductListAdapter.ProductListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View productListView = LayoutInflater.from(productListContext).inflate(R.layout.product_list_item, parent, false);
        return new ProductListAdapter.ProductListViewHolder(productListView);
    }

    @Override
    public void onBindViewHolder(ProductListViewHolder holder, final int position) {
        ProductList productlist = productList.get(position);

        holder.productCode.setText("Code: "+productlist.getProductCode());
        holder.productName.setText(""+productlist.getProductName());
        holder.productQuantity.setText("Quantity: "+productlist.getProductQuantity()+" Kg.");
        holder.productPurchasePrice.setText("Purchase Price: "+productlist.getProductPurchasePrice()+" Tk.");
        holder.productSellingPrice.setText("Sell Price: "+productlist.getProductSellingPrice()+" Tk.");

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


}
