package sizramsolutions.sizrambookkeeping.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.sizrambookkeeping.Models.PurchaseList;
import sizramsolutions.sizrambookkeeping.R;


public class PurchaseListAdapter extends RecyclerView.Adapter<PurchaseListAdapter.PurchaseListViewHolder> {
        private List<PurchaseList> purchaseList;
        private Context purchaseListContext;

        public PurchaseListAdapter(List<PurchaseList> purchaseList, Context purchaseListContext) {
            this.purchaseList = purchaseList;
            this.purchaseListContext = purchaseListContext;

        }

        public class PurchaseListViewHolder extends RecyclerView.ViewHolder {

            public TextView vendorName, purchaseDate, purchaseInvoice, purchaseAmount, purchasePaid, purchaseBalance;

            public PurchaseListViewHolder(View view) {

                super(view);
                vendorName = (TextView) view.findViewById(R.id.vendor_name);
                purchaseDate = (TextView) view.findViewById(R.id.purchase_date);
                purchaseInvoice = (TextView) view.findViewById(R.id.purchase_invoice);
                purchaseAmount = (TextView) view.findViewById(R.id.purchase_amount);
                purchasePaid = (TextView) view.findViewById(R.id.purchase_paid);
                purchaseBalance = (TextView) view.findViewById(R.id.purchase_balance);

            }

        }


        @Override
        public PurchaseListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View purchaseListView = LayoutInflater.from(purchaseListContext).inflate(R.layout.purchase_list_item, parent, false);
            return new PurchaseListViewHolder(purchaseListView);
        }

        @Override
        public void onBindViewHolder(PurchaseListViewHolder holder, final int position) {
            PurchaseList purchaselist = purchaseList.get(position);

            holder.vendorName.setText("" + purchaselist.getVendorName());
            holder.purchaseDate.setText("" + purchaselist.getPurchaseDate());
            holder.purchaseInvoice.setText("Invoice: " + purchaselist.getPurchaseInvoice());
            holder.purchaseAmount.setText("Total: " + purchaselist.getPurchaseAmount() + " Tk.");
            holder.purchasePaid.setText("Paid: " + purchaselist.getPurchasePaid() + " Tk.");
            holder.purchaseBalance.setText("Due: " + purchaselist.getPurchaseBalance() + " Tk.");

        }

        @Override
        public int getItemCount() {
            return purchaseList.size();
        }
    }