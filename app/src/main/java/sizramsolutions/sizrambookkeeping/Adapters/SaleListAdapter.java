package sizramsolutions.sizrambookkeeping.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.sizrambookkeeping.Models.SaleList;
import sizramsolutions.sizrambookkeeping.R;

public class SaleListAdapter extends RecyclerView.Adapter<SaleListAdapter.SaleListViewHolder> {
    private List<SaleList> saleList;
    private Context saleContext;

    public SaleListAdapter(List<SaleList> saleList, Context saleContext){
        this.saleList = saleList;
        this.saleContext = saleContext;

    }

    public class SaleListViewHolder extends RecyclerView.ViewHolder{

        public TextView customerName, saleDate, saleInvoice, saleAmount, salePaid, saleBalance;

        public SaleListViewHolder(View view){

            super(view);

            customerName = (TextView)view.findViewById(R.id.customer_name);
            saleDate = (TextView)view.findViewById(R.id.sale_date);
            saleInvoice = (TextView)view.findViewById(R.id.sale_invoice);
            saleAmount = (TextView)view.findViewById(R.id.sale_amount);
            salePaid = (TextView)view.findViewById(R.id.sale_paid);
            saleBalance = (TextView)view.findViewById(R.id.sale_due);

        }

    }


    @Override
    public SaleListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View salelistView = LayoutInflater.from(saleContext).inflate(R.layout.sale_list_item, parent, false);
        return new SaleListViewHolder(salelistView);
    }

    @Override
    public void onBindViewHolder(SaleListViewHolder holder, final int position) {
        SaleList salelist = saleList.get(position);

        holder.customerName.setText(""+ salelist.getCustomerName());
        holder.saleDate.setText(""+ salelist.getSaleDate());
        holder.saleInvoice.setText("Invoice: "+ salelist.getSaleInvoice());
        holder.saleAmount.setText("Total: "+ salelist.getSaleAmount()+" Tk.");
        holder.salePaid.setText("Paid: " + salelist.getSalePaid()+" Tk.");
        holder.saleBalance.setText("Due: " + salelist.getSaleBalance()+" Tk.");

    }

    @Override
    public int getItemCount() {
        return saleList.size();
    }
}
