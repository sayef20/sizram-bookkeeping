package sizramsolutions.sizrambookkeeping.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sizramsolutions.sizrambookkeeping.Fragments.ScreenSlideFragment;
import sizramsolutions.sizrambookkeeping.R;

public class ScreenSlideAdapter extends FragmentPagerAdapter{

    String[] screenDescription;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public ScreenSlideAdapter(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();
        screenDescription =resources.getStringArray(R.array.screen_description);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getScreenShots(position));
        bundle.putString(DescriptionKey, screenDescription[position]);
        ScreenSlideFragment screenSlideFragment = new ScreenSlideFragment();
        screenSlideFragment.setArguments(bundle);

        return screenSlideFragment;
    }

    private int getScreenShots(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.screen1;
                break;
            case 1:
                id = R.drawable.screen2;
                break;
            case 2:
                id = R.drawable.screen3;
                break;
            case 3:
                id = R.drawable.screen4;
                break;
        }
        return id;

    }

    public int getCount() {
        return screenDescription.length;
    }

}
