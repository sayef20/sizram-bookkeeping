package sizramsolutions.sizrambookkeeping.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sizramsolutions.sizrambookkeeping.Fragments.TabThisWeekFragment;
import sizramsolutions.sizrambookkeeping.Fragments.TabThisMonthFragment;
import sizramsolutions.sizrambookkeeping.Fragments.TabTodayFragment;

import static sizramsolutions.sizrambookkeeping.Fragments.DashboardFragment.tab;

public class TabPagerAdapter extends FragmentPagerAdapter {


    public TabPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new TabTodayFragment();
            case 1:
                return new TabThisWeekFragment();
            case 2:
                return new TabThisMonthFragment();




        }
        return null;
    }

    @Override
    public int getCount() {


        return tab;
    }

    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "Today";
            case 1:
                return "This Week";
            case 2:
                return "This Month";

        }

        return null;
    }

}
