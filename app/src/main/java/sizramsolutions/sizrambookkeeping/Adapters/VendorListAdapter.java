package sizramsolutions.sizrambookkeeping.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Models.VendorList;
import sizramsolutions.sizrambookkeeping.R;


public class VendorListAdapter extends RecyclerView.Adapter<VendorListAdapter.VendorListViewHolder> {
    private List<VendorList> vendorList;
    Context vendorListContext;

    public VendorListAdapter(List<VendorList> vendorList, Context vendorListContext){
        this.vendorList = vendorList;
        this.vendorListContext = vendorListContext;

    }


    public class VendorListViewHolder extends RecyclerView.ViewHolder{

        public TextView vendorName, vendorPhone, vendorLastTransaction, vendorDue;

        public VendorListViewHolder(View view){
            super(view);

            vendorName = (TextView)view.findViewById(R.id.vendor_name);
            vendorPhone = (TextView)view.findViewById(R.id.vendor_phone);
            vendorLastTransaction = (TextView)view.findViewById(R.id.last_vendor_transaction);
            vendorDue = (TextView)view.findViewById(R.id.vendor_due);

        }

    }


    @Override
    public VendorListAdapter.VendorListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vendorListView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vendor_list_item, parent, false);
        return new VendorListAdapter.VendorListViewHolder(vendorListView);
    }


    @Override
    public void onBindViewHolder(VendorListAdapter.VendorListViewHolder holder, final int position) {
        VendorList vendorlist = vendorList.get(position);

        View customerListView = holder.itemView;

        holder.vendorName.setText(""+ vendorlist.getVendorName());
        holder.vendorPhone.setText("Phone: "+ vendorlist.getVendorPhone());
        holder.vendorLastTransaction.setText("Last: "+ vendorlist.getVendorLastTransaction()+" Tk.");
        holder.vendorDue.setText("Balance: " + vendorlist.getVendorDue()+" Tk.");

    }

    @Override
    public int getItemCount() {
        return vendorList.size();
    }
}
