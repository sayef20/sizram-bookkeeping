package sizramsolutions.sizrambookkeeping.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Activities.DashboardActivity;
import sizramsolutions.sizrambookkeeping.Adapters.CustomerListAdapter;
import sizramsolutions.sizrambookkeeping.Models.CustomerList;
import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class CustomerListFragment extends Fragment {

    private static final String TAG = CustomerListFragment.class.getSimpleName();

    private List<CustomerList> customerlist = new ArrayList<>();
    private CustomerListAdapter customerListAdapter;
    private RecyclerView customerRecycler;

    RequestQueue requestQueue;

    private static final String URL_CUSTOMERS = "http://ss-app01.com/shinutrade/index.php/Android_api/get_customers";

    public CustomerListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_customer_list, container, false);

        RecyclerView customerRecycler = (RecyclerView)view.findViewById(R.id.customer_list);
        customerRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        customerListAdapter = new CustomerListAdapter(customerlist, getContext());
        customerRecycler.setAdapter(customerListAdapter);

        customerData();

        return view;
    }

    private void customerData(){

        // Loading Progress Dialogue //

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_CUSTOMERS,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray customerArray) {

                           for(int i = 0; i<customerArray.length(); i++){
                               try{

                               JSONObject customerJsonObject = customerArray.getJSONObject(i);

                                   CustomerList customerList = new CustomerList(
                                           customerJsonObject.getInt("customer_id"),
                                           customerJsonObject.optString("customer_company_name", "Not Found"),
                                           customerJsonObject.optString("customer_contact1", "Not Found"),
                                           customerJsonObject.optDouble("customer_last_payment", 0),
                                           customerJsonObject.optDouble("customer_current_balance", 0)


                                           );

                                   customerlist.add(customerList);


                       }catch (JSONException e){

                            e.printStackTrace();
                           Toast.makeText(getContext(),
                                   "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                       }finally {
                                   customerListAdapter.notifyItemChanged(i);
                               }

                               }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                       "Error Occurred: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((DashboardActivity) getActivity())
                .setActionBarTitle("Customer List");
        super.onResume();
    }
}
