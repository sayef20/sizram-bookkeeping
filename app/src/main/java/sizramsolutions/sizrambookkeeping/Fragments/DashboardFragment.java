package sizramsolutions.sizrambookkeeping.Fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sizramsolutions.sizrambookkeeping.Activities.DashboardActivity;
import sizramsolutions.sizrambookkeeping.Adapters.TabPagerAdapter;
import sizramsolutions.sizrambookkeeping.R;


public class DashboardFragment extends Fragment {


    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int tab= 3;



    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard,null);
        tabLayout=(TabLayout)v.findViewById(R.id.tabs);
        viewPager=(ViewPager)v.findViewById(R.id.viewpager);
        //set an adpater

        viewPager.setAdapter(new TabPagerAdapter( getChildFragmentManager()));

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        ((DashboardActivity) getActivity())
                .setActionBarTitle("Dashboard");
        super.onResume();
    }


}
