package sizramsolutions.sizrambookkeeping.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Adapters.ProductListAdapter;
import sizramsolutions.sizrambookkeeping.Models.ProductList;
import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class LowStockFragment extends Fragment {

    private static final String TAG = LowStockFragment.class.getSimpleName();

    private List<ProductList> lowproductlist = new ArrayList<>();
    private ProductListAdapter productListAdapter;
    private RecyclerView productRecycler;

    RequestQueue requestQueue;

    private static final String URL_PRODUCT_LIST = "http://ss-app01.com/shinutrade/index.php/Android_api/get_inventory";




    public LowStockFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_low_stock, container, false);

        RecyclerView productRecycler = (RecyclerView)view.findViewById(R.id.low_stock_list);
        productRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        productListAdapter = new ProductListAdapter(lowproductlist, getContext());
        productRecycler.setAdapter(productListAdapter);

        lowProductData();

        return view;

    }

    private void lowProductData() {


        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_PRODUCT_LIST,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray lowProductArray) {

                        for(int i = 0; i<lowProductArray.length(); i++){
                            try{

                                JSONObject productJasonObject = lowProductArray.getJSONObject(i);

                             //   if(productJasonObject.getDouble("product_stock") < 100.0){

                                    ProductList lowProductList = new ProductList(

                                            productJasonObject.getInt("product_id"),
                                            productJasonObject.optInt("product_code", 0),
                                            productJasonObject.optDouble("product_stock", 0),
                                            productJasonObject.optString("product_name", "Not Found"),
                                            productJasonObject.optDouble("product_purchase_price",0),
                                            productJasonObject.optDouble("product_sell_price",0)

                                    );

                                    lowproductlist.add(lowProductList);

                          //      }


                            }catch (JSONException e){

                                e.printStackTrace();
                                Toast.makeText(getContext(),
                                        "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }finally {

                                productListAdapter.notifyItemChanged(i);
                            }

                        }

                   }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        "Error Occurred: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);


    }

}
