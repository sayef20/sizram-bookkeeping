package sizramsolutions.sizrambookkeeping.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Activities.DashboardActivity;
import sizramsolutions.sizrambookkeeping.Adapters.ProductListAdapter;
import sizramsolutions.sizrambookkeeping.Adapters.PurchaseListAdapter;
import sizramsolutions.sizrambookkeeping.Adapters.SaleListAdapter;
import sizramsolutions.sizrambookkeeping.Models.ProductList;
import sizramsolutions.sizrambookkeeping.Models.PurchaseList;
import sizramsolutions.sizrambookkeeping.Models.SaleList;
import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class ProductListFragment extends Fragment {

    private static final String TAG = ProductListFragment.class.getSimpleName();

    private List<ProductList> productlist = new ArrayList<>();
    private ProductListAdapter productListAdapter;
    private RecyclerView productRecycler;

    RequestQueue requestQueue;

    private static final String URL_PRODUCT_LIST = "http://ss-app01.com/shinutrade/index.php/Android_api/get_inventory";


    public ProductListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        RecyclerView productRecycler = (RecyclerView)view.findViewById(R.id.product_list);
        productRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        productListAdapter = new ProductListAdapter(productlist, getContext());
        productRecycler.setAdapter(productListAdapter);

        productData();

        return view;
    }

    private void productData() {


        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_PRODUCT_LIST,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray productArray) {

                        for(int i = 0; i<productArray.length(); i++){
                            try{

                                JSONObject productJasonObject = productArray.getJSONObject(i);

                                ProductList productList = new ProductList(

                                            productJasonObject.getInt("product_id"),
                                                    productJasonObject.optInt("product_code", 0),
                                                    productJasonObject.optDouble("product_stock", 0),
                                                    productJasonObject.optString("product_name", "Not Found"),
                                                    productJasonObject.optDouble("product_purchase_price",0),
                                                    productJasonObject.optDouble("product_sell_price",0)

                                );

                                productlist.add(productList);


                            }catch (JSONException e){

                                e.printStackTrace();
                                Toast.makeText(getContext(),
                                        "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }finally {

                                productListAdapter.notifyItemChanged(i);
                            }

                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        "Error Occurred: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((DashboardActivity) getActivity())
                .setActionBarTitle("Stock List");
        super.onResume();
    }
}
