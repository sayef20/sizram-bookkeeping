package sizramsolutions.sizrambookkeeping.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Activities.DashboardActivity;
import sizramsolutions.sizrambookkeeping.Adapters.CustomerListAdapter;
import sizramsolutions.sizrambookkeeping.Adapters.SaleListAdapter;
import sizramsolutions.sizrambookkeeping.Models.CustomerList;
import sizramsolutions.sizrambookkeeping.Models.SaleList;
import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class SaleFragment extends Fragment {

    private static final String TAG = SaleFragment.class.getSimpleName();

    private List<SaleList> salelist = new ArrayList<>();
    private SaleListAdapter saleListAdapter;
    private RecyclerView saleRecycler;

    RequestQueue requestQueue;

    private static final String URL_SALE = "http://ss-app01.com/shinutrade/index.php/Android_api/get_sells";



    public SaleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_sale, container, false);

        RecyclerView saleRecycler = (RecyclerView)view.findViewById(R.id.sale_list);
        saleRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        saleListAdapter = new SaleListAdapter(salelist, getContext());
        saleRecycler.setAdapter(saleListAdapter);

        saleData();

        return view;
    }

    private void saleData(){

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_SALE,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray saleArray) {

                        for(int i = 0; i<saleArray.length(); i++){
                            try{

                                JSONObject saleJsonObject = saleArray.getJSONObject(i);

                                SaleList saleList = new SaleList(
                                        saleJsonObject.getLong("sell_id"),
                                        saleJsonObject.optString("sell_date", "Not Found"),
                                        saleJsonObject.optString("customer_company_name", "Not Found"),
                                        saleJsonObject.optInt("invoice_number", 0),
                                        saleJsonObject.optDouble("total_price", 0),
                                        saleJsonObject.optDouble("invoice_paid_amount", 0),
                                        saleJsonObject.optDouble("invoice_due", 0)


                                );

                                salelist.add(saleList);


                            }catch (JSONException e){

                                e.printStackTrace();
                                Toast.makeText(getContext(),
                                        "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }finally {

                                saleListAdapter.notifyItemChanged(i);
                            }

                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        "Error Occurred: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((DashboardActivity) getActivity())
                .setActionBarTitle("Sales List");
        super.onResume();
    }

}
