package sizramsolutions.sizrambookkeeping.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import sizramsolutions.sizrambookkeeping.R;


public class ScreenSlideFragment extends Fragment {

    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";

    public ScreenSlideFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.start_fragment_layout, container, false);

        Bundle bundle = getArguments();
        if(bundle != null){

            int imageID = bundle.getInt(ImageIDKey);
            String description = bundle.getString(DescriptionKey);

            setValues(view, imageID, description);
        }
        return view;
    }

    private void setValues(View view, int imageID, String description) {
        ImageView imageview = view.findViewById(R.id.image_view);
        imageview.setImageResource(imageID);


        TextView textview = view.findViewById(R.id.text_view);
        textview.setText(Html.fromHtml(description));
    }

}
