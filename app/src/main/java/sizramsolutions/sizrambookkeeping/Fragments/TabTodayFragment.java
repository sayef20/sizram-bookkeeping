package sizramsolutions.sizrambookkeeping.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class TabTodayFragment extends Fragment {

    private static final String TAG = TabTodayFragment.class.getSimpleName();

    TextView totalSell, totalPurchase, totalReceived, totalPaid, totalReceivable, totalPayable, totalExpense, totalStock;

    double sellAmount, purchaseAmount, receivedAmount, paidAmount, receivableAmount, payableAmount, stockAmount, expenseAmount, openingUnitAmount, openingItemAmount, purchaseUnitAmount, purchaseItemAmount, saleUnitAmount, saleItemAmount;

    RequestQueue requestQueue;

    private static final String URL_TODAY_TRANSACTION = "http://ss-app01.com/shinutrade/index.php/Android_api/today_transaction";


    PieChart pieChart;
    List<PieEntry> pieEntries = new ArrayList<>();

    public TabTodayFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_today, container, false);

        totalSell = view.findViewById(R.id.total_sell_amount);
        totalPurchase = view.findViewById(R.id.total_purchase_amount);
        totalReceived = view.findViewById(R.id.total_received_amount);
        totalPaid = view.findViewById(R.id.total_paid_amount);
        totalReceivable = view.findViewById(R.id.total_receivable_amount);
        totalPayable = view.findViewById(R.id.total_payable_amount);
        totalStock = view.findViewById(R.id.total_stock_amount);
        totalExpense = view.findViewById(R.id.total_expense_amount);

        pieChart = (PieChart) view.findViewById(R.id.today_chart);


        SetUpPiechart();

        TodayTransactionData();

        return view;
    }

    private void SetUpPiechart() {

        pieChart.getDescription().setEnabled(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(.95f);
        pieChart.setDrawHoleEnabled(false);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(30f);
        pieChart.setEntryLabelColor(Color.rgb(12,30,29));

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_TODAY_TRANSACTION,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray todayTransactionArray) {

                        for(int i =0; i < todayTransactionArray.length(); i++){

                            try {
                                JSONObject todayTransactionObject = todayTransactionArray.getJSONObject(i);

                                sellAmount = todayTransactionObject.optDouble("selltotal", 0);
                                purchaseAmount = todayTransactionObject.optDouble("purchasetotal", 0);
                                receivedAmount = todayTransactionObject.optDouble("receivetotal", 0);
                                paidAmount = todayTransactionObject.optDouble("vendor_paidtotal", 0);
                                receivableAmount = (sellAmount - receivedAmount);
                                payableAmount = (purchaseAmount- paidAmount);

                                openingUnitAmount = todayTransactionObject.optDouble("opening_unit_total", 0);
                                openingItemAmount = todayTransactionObject.optDouble("opening_item_total", 0);
                                purchaseUnitAmount = todayTransactionObject.optDouble("purchase_unit_total", 0);
                                purchaseItemAmount = todayTransactionObject.optDouble("purchase_item_total", 0);
                                saleUnitAmount = todayTransactionObject.optDouble("sell_unit_total", 0);
                                saleItemAmount = todayTransactionObject.optDouble("sell_item_total", 0);
                                stockAmount = ((openingUnitAmount*openingItemAmount + purchaseUnitAmount*purchaseItemAmount)-(saleUnitAmount*saleItemAmount));

                                expenseAmount = todayTransactionObject.optDouble("todays_expense", 0);

                                float transactionAmount[] = {(float)sellAmount,(float)purchaseAmount,(float)receivedAmount,(float)paidAmount,(float)receivableAmount,(float)payableAmount,(float)expenseAmount,(float)stockAmount};
                                String transactionCategory[] = {"Total Sales", "Total Purchases", "Total Received", "Total Paid", "Total Receivable", "Total Payable", "Total Expense", "Total Stock"};

                                //You should clear your pie Entries list because every time you call Api
                                //it just adds up to your previous data making your Pie graph very messy
                                //that's why a little addition to your code. pieEntries.clear()  ---Tahmid

                                pieEntries.clear();
                                for(int j =0; j < transactionAmount.length; j++){

                                    pieEntries.add(new PieEntry(transactionAmount[j], transactionCategory[j]));
                                }

                                pieChart.animateY(500, Easing.EasingOption.EaseInBack);

                                PieDataSet pieDataset = new PieDataSet(pieEntries,"");
                                pieDataset.setSliceSpace(3f);
                                pieDataset.setSelectionShift(5f);
                                pieDataset.setColors(ColorTemplate.MATERIAL_COLORS);
                                pieDataset.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

                                PieData pieData = new PieData(pieDataset);
                                pieData.setValueTextSize(4f);
                                pieData.setValueTextColor(Color.WHITE);



                                pieChart.setData(pieData);



                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(),
                                        "exception Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        "Response Error: " + error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }


    private void TodayTransactionData() {

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_TODAY_TRANSACTION,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray todayTransactionArray) {

                        for(int i =0; i < todayTransactionArray.length(); i++){

                            try {
                                JSONObject todayTransactionObject = todayTransactionArray.getJSONObject(i);

                                sellAmount = todayTransactionObject.optDouble("selltotal", 0);
                                 purchaseAmount = todayTransactionObject.optDouble("purchasetotal", 0);
                                receivedAmount = todayTransactionObject.optDouble("receivetotal", 0);
                                paidAmount = todayTransactionObject.optDouble("vendor_paidtotal", 0);
                                receivableAmount = (sellAmount - receivedAmount);
                                payableAmount = (purchaseAmount- paidAmount);

                                openingUnitAmount = todayTransactionObject.optDouble("opening_unit_total", 0);
                                openingItemAmount = todayTransactionObject.optDouble("opening_item_total", 0);
                                purchaseUnitAmount = todayTransactionObject.optDouble("purchase_unit_total", 0);
                                purchaseItemAmount = todayTransactionObject.optDouble("purchase_item_total", 0);
                                saleUnitAmount = todayTransactionObject.optDouble("sell_unit_total", 0);
                                saleItemAmount = todayTransactionObject.optDouble("sell_item_total", 0);
                                stockAmount = ((openingUnitAmount*openingItemAmount + purchaseUnitAmount*purchaseItemAmount)-(saleUnitAmount*saleItemAmount));

                                expenseAmount = todayTransactionObject.optDouble("todays_expense", 0);


                                totalSell.setText(""+ sellAmount+" Tk.");
                                totalPurchase.setText(""+purchaseAmount+" Tk.");
                                totalReceived.setText(""+receivedAmount+" Tk.");
                                totalPaid.setText(""+paidAmount+" Tk.");

                                String receivableTotal = Double.toString(receivableAmount);
                                totalReceivable.setText(""+receivableTotal+" Tk.");

                                String payableTotal = Double.toString(payableAmount);
                                totalPayable.setText(""+payableTotal+" Tk.");

                                totalExpense.setText(""+expenseAmount+" Tk.");

                                String stockTotal = Double.toString(stockAmount);
                                totalStock.setText(""+stockTotal+" Tk.");

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(),
                                        "exception Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        "Response Error: " + error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }

}
