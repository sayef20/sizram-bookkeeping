package sizramsolutions.sizrambookkeeping.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.sizrambookkeeping.Activities.DashboardActivity;
import sizramsolutions.sizrambookkeeping.Adapters.CustomerListAdapter;
import sizramsolutions.sizrambookkeeping.Adapters.VendorListAdapter;
import sizramsolutions.sizrambookkeeping.Models.CustomerList;
import sizramsolutions.sizrambookkeeping.Models.VendorList;
import sizramsolutions.sizrambookkeeping.R;
import sizramsolutions.sizrambookkeeping.utils.AppController;

public class VendorListFragment extends Fragment {
    private static final String TAG = VendorListFragment.class.getSimpleName();

    private List<VendorList> vendorlist = new ArrayList<>();
    private VendorListAdapter vendorListAdapter;
    private RecyclerView vendorRecycler;

    RequestQueue requestQueue;

    private static final String URL_VENDORS = "http://ss-app01.com/shinutrade/index.php/Android_api/get_vendors";

    public VendorListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_vendor_list, container, false);

        RecyclerView vendorRecycler = (RecyclerView)view.findViewById(R.id.vendor_list);
        vendorRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        vendorListAdapter = new VendorListAdapter(vendorlist, getContext());
        vendorRecycler.setAdapter(vendorListAdapter);

        vendorData();

        return view;
    }

    private void vendorData(){

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_VENDORS,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray vendorArray) {

                        for(int i = 0; i<vendorArray.length(); i++){
                            try{

                                JSONObject vendorJsonObject = vendorArray.getJSONObject(i);

                                VendorList vendorList = new VendorList(
                                        vendorJsonObject.getInt("vendor_id"),
                                        vendorJsonObject.optString("vendor_company_name", "Not Found"),
                                        vendorJsonObject.optString("vendor_contact1", "Not Found"),
                                        vendorJsonObject.optDouble("vendor_last_payment", 0),
                                        vendorJsonObject.optDouble("vendor_current_balance", 0)


                                );

                                vendorlist.add(vendorList);


                            }catch (JSONException e){

                                e.printStackTrace();
                                Toast.makeText(getContext(),
                                        "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }finally {
                                vendorListAdapter.notifyItemChanged(i);
                            }

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        "Error Occurred: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);


    }

    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((DashboardActivity) getActivity())
                .setActionBarTitle("Vendor List");
        super.onResume();
    }

}
