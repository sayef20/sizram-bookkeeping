package sizramsolutions.sizrambookkeeping.Models;


public class CustomerDetails {

    private String customerTransactionDate;
    private int customerTransactionInvoiceNo;
    private double customerTransactionAmount, customerTransactionPaid, customerTransactionDue;

    public CustomerDetails(){

    }

    public CustomerDetails(String customerTransactionDate, int customerTransactionInvoiceNo, double customerTransactionAmount, double customerTransactionPaid, double customerTransactionDue){

        this.customerTransactionDate = customerTransactionDate;
        this.customerTransactionInvoiceNo = customerTransactionInvoiceNo;
        this.customerTransactionAmount = customerTransactionAmount;
        this.customerTransactionPaid = customerTransactionPaid;
        this.customerTransactionDue = customerTransactionDue;

    }

    public String getCustomerTransactionDate() {
        return customerTransactionDate;
    }

    public void setCustomerTransactionDate(String  customerTransactionDate) {
        this.customerTransactionDate = customerTransactionDate;
    }

    public int getCustomerTransactionInvoiceNo() {
        return customerTransactionInvoiceNo;
    }

    public void setCustomerTransactionInvoiceNo(int customerTransactionInvoiceNo) {
        this.customerTransactionInvoiceNo = customerTransactionInvoiceNo;
    }

    public double getCustomerTransactionAmount() {
        return customerTransactionAmount;
    }

    public void setCustomerTransactionAmount(double customerTransactionAmount) {
        this.customerTransactionAmount = customerTransactionAmount;
    }

    public double getCustomerTransactionPaid() {
        return customerTransactionPaid;
    }

    public void setCustomerTransactionPaid(double customerTransactionPaid) {
        this.customerTransactionPaid = customerTransactionPaid;
    }

    public double getCustomerTransactionDue() {
        return customerTransactionDue;
    }

    public void setCustomerTransactionDue(double customerTransactionDue) {
        this.customerTransactionDue = customerTransactionDue;
    }
}
