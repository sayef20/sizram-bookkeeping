package sizramsolutions.sizrambookkeeping.Models;


public class CustomerList {

    private int customerId;
    private String customerName, customerPhone;
    private double customerLastTransaction, customerDue;


    public CustomerList(){

    }

    public  CustomerList(int customerId, String customerName, String customerPhone, double customerLastTransaction, double customerDue){

        this.customerId = customerId;
        this.customerName = customerName;
        this.customerPhone = customerPhone;
        this.customerLastTransaction = customerLastTransaction;
        this.customerDue = customerDue;

    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public double getCustomerLastTransaction() {
        return customerLastTransaction;
    }

    public void setCustomerLastTransaction(double customerLastTransaction) {
        this.customerLastTransaction = customerLastTransaction;
    }

    public double getCustomerDue() {
        return customerDue;
    }

    public void setCustomerDue(double customerDue) {
        this.customerDue = customerDue;
    }
}
