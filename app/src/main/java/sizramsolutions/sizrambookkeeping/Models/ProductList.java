package sizramsolutions.sizrambookkeeping.Models;

public class ProductList {
    private int productId, productCode;
    private String productName;
    private double  productQuantity, productPurchasePrice, productSellingPrice;

    public ProductList(){

    }

    public ProductList(int productId, int productCode, double productQuantity, String productName, double productPurchasePrice, double productSellingPrice){

        this.productId = productId;
        this.productCode = productCode;
        this.productQuantity = productQuantity;
        this.productName = productName;
        this.productPurchasePrice = productPurchasePrice;
        this.productSellingPrice = productSellingPrice;

    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public double getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(double productQuantity) {
        this.productQuantity = productQuantity;
    }
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPurchasePrice() {
        return productPurchasePrice;
    }

    public void setProductPurchasePrice(double productPurchasePrice) {
        this.productPurchasePrice = productPurchasePrice;
    }

    public double getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(double productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }
}


