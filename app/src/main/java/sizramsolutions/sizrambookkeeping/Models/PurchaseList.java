package sizramsolutions.sizrambookkeeping.Models;


public class PurchaseList {
    private long purchaseId;
    private int purchaseInvoice;
    private String vendorName, purchaseDate;
    private double purchaseAmount, purchasePaid, purchaseBalance;
    public PurchaseList(){

    }

    public PurchaseList(long purchaseId, int purchaseInvoice, String vendorName, String purchaseDate, double purchaseAmount, double purchasePaid, double purchaseBalance){

        this.purchaseId =purchaseId;
        this.vendorName = vendorName;
        this.purchaseDate = purchaseDate;
        this.purchaseInvoice = purchaseInvoice;
        this.purchaseAmount = purchaseAmount;
        this.purchasePaid = purchasePaid;
        this.purchaseBalance = purchaseBalance;
    }

    public long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getPurchaseInvoice() {
        return purchaseInvoice;
    }

    public void setPurchaseInvoice(int purchaseInvoice) {
        this.purchaseInvoice = purchaseInvoice;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public double getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(double purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public double getPurchasePaid() {
        return purchasePaid;
    }

    public void setPurchasePaid(double purchasePaid) {
        this.purchasePaid = purchasePaid;
    }

    public double getPurchaseBalance() {
        return purchaseBalance;
    }

    public void setPurchaseBalance(double purchaseBalance) {
        this.purchaseBalance = purchaseBalance;
    }
}
