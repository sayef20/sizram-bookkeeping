package sizramsolutions.sizrambookkeeping.Models;


public class SaleList {

    private long saleId;
    private int saleInvoice;
    private String customerName, saleDate;
    private double saleAmount, salePaid, saleBalance;

    public SaleList(){

    }

    public SaleList(long saleId, String saleDate, String customerName, int saleInvoice, double saleAmount, double salePaid, double saleBalance){
        this.saleId = saleId;
        this.saleDate = saleDate;
        this.customerName = customerName;
        this.saleInvoice = saleInvoice;
        this.saleAmount = saleAmount;
        this.salePaid = salePaid;
        this.saleBalance = saleBalance;
    }

    public long getSaleId() {
        return saleId;
    }

    public void setSaleId(long saleId) {
        this.saleId = saleId;
    }

    public int getSaleInvoice() {
        return saleInvoice;
    }

    public void setSaleInvoice(int saleInvoice) {
        this.saleInvoice = saleInvoice;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public double getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(double saleAmount) {
        this.saleAmount = saleAmount;
    }

    public double getSalePaid() {
        return salePaid;
    }

    public void setSalePaid(double salePaid) {
        this.salePaid = salePaid;
    }
    public double getSaleBalance() {
        return saleBalance;
    }

    public void setSaleBalance(double saleBalance) {
        this.saleBalance = saleBalance;
    }
}
