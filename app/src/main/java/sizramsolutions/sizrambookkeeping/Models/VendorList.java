package sizramsolutions.sizrambookkeeping.Models;

public class VendorList {

    private int vendorId;
    private String vendorName, vendorPhone;
    private double vendorLastTransaction, vendorDue;


    public VendorList(){

    }

    public  VendorList(int vendorId, String vendorName, String vendorPhone, double vendorLastTransaction, double vendorDue){

        this.vendorId = vendorId;
        this.vendorName = vendorName;
        this.vendorPhone = vendorPhone;
        this.vendorLastTransaction = vendorLastTransaction;
        this.vendorDue = vendorDue;

    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorPhone() {
        return vendorPhone;
    }

    public void setVendorPhone(String vendorPhone) {
        this.vendorPhone = vendorPhone;
    }

    public double getVendorLastTransaction() {
        return vendorLastTransaction;
    }

    public void setVendorLastTransaction(double vendorLastTransaction) {
        this.vendorLastTransaction = vendorLastTransaction;
    }

    public double getVendorDue() {
        return vendorDue;
    }

    public void setVendorDue(double vendorDue) {
        this.vendorDue = vendorDue;
    }
}
